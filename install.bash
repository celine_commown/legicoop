#! /bin/bash

set -e

SCRIPT_DIR=$(cd $(dirname "${BASH_SOURCE[0]}") && pwd)
SCRIPT_NAME="$0"
ARGS=( "$@" )
BRANCH="master"


self_update() {
    cd "$SCRIPT_DIR"
    echo "==== Recherche d'une nouvelle version..."
    git fetch origin

    [ ! "$(git rev-parse HEAD)"="$(git rev-parse origin/${BRANCH})" ] && {
        echo "==== Une nouvelle version a été trouvée. Mise à jour en cours..."
        git pull --force
        git checkout "$BRANCH"
        git pull --force
        echo "==== FAIT !"
        echo
        echo "==== Exécution de la nouvelle version..."
        cd - > /dev/null
        exec "$SCRIPT_NAME" "${ARGS[@]}"
        exit 1
    }

    echo "Déjà la version la plus à jour."
    echo
}

echo "==== Installation des pré-requis..."
which git > /dev/null || sudo DEBIAN_FRONTEND=noninteractive apt install -y git rsync
echo "==== FAIT !"
echo

self_update

echo "==== Copie des données (dure plusieurs minutes)..."
sudo rsync -r --info=progress2 "${SCRIPT_DIR}"/data /
echo "==== FAIT !"
echo

echo "==== Installation de logiciels supplémentaires..."
sudo snap install onlyoffice-desktopeditors

sudo DEBIAN_FRONTEND=noninteractive add-apt-repository -y ppa:nextcloud-devs/client
sudo DEBIAN_FRONTEND=noninteractive apt install -y \
     thunderbird \
     nautilus-nextcloud \
     keepassxc webext-keepassxc-browser

# Virtualbox requires an EULA, so we cannot use noninteractive here:
sudo apt install -y \
     virtualbox virtualbox-guest-additions-iso virtualbox-ext-pack \

echo "==== TERMINÉ !"
